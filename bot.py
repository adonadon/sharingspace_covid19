from telegram.ext import *
import requests
import re

def get_url():	
	contents = requests.get('https://random.dog/woof.json').json()
	url = contents['url']
	return url

def bop(bot, update):
	# url = get_url()
	chat_id = update.message.chat_id
	# bot.send_photo(chat_id=chat_id, photo=url)
	pic = "https://bitcoin.org/img/icons/opengraph.png"
	txt = "hola"
	bot.send_message(chat_id, txt)
	bot.send_photo(chat_id=chat_id,photo=pic)

def guille(bot, update):
	chat_id = update.message.chat_id
	txt = "Guille es tonto"
	bot.send_message(chat_id, txt)
	bot.send_photo(chat_id=chat_id, photo=open("/home/andre/Desktop/guille.png","rb"))

def locate(bot, update):
	chat_id = bot.get_updates()[-1].message.chat_id
	location_keyboard = telegram.KeyboardButton(text="send_location", request_location=True)
	contact_keyboard = telegram.KeyboardButton(text="send_contact", request_contact=True)
	custom_keyboard = [[ location_keyboard, contact_keyboard ]]
	reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
	bot.send_message(chat_id=chat_id, text="Would you mind sharing your location and contact with me?", reply_markup=reply_markup)


def location(bot,update):
	chat_id = update.message.chat_id
	message=None
	if update.edited_message:
		message = update.edited_message
	else:
		message = update.message
	current_pos = (message.location.latitude, message.location.longitude)
	bot.sendLocation(chat_id,message.location.latitude, message.location.longitude,60)
	print(current_pos, message.from_user.first_name)
	

def text(bot, update):
	updates = bot.get_updates()
	print([u.message.text for u in updates])
	print("\nMessage length is", len(updates))


def main():
	updater = Updater('1236392751:AAETfJqBWSMGpyQ5DMPwjYYTGwKFXYNuhug')
	dp = updater.dispatcher
	dp.add_handler(CommandHandler('bop',bop))
	dp.add_handler(CommandHandler('guille',guille))
	dp.add_handler(CommandHandler('locate',locate))
	dp.add_handler(MessageHandler(Filters.text,text))
	dp.add_handler(MessageHandler(Filters.location, location))
	updater.start_polling()
	updater.idle()

if __name__ == '__main__':
	main()
